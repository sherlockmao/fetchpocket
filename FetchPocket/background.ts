// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Global accessor that the popup uses.

function nowb() {
    var now = new Date();
    return now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
}

console.log(nowb() + " background loaded again");

var cancelled = true;
var demandNext = true;
var thetab = -1;

function start() {
    cancelled = false;
    demandNext = true;
    chrome.tabs.sendMessage(thetab, "Next");
}

function cancel() {
    cancelled = true;
}

function handleArticle(art: Article) {

    var id = chrome.storage.local[art.id];

    if (!id) {

        chrome.storage.local[art.id] = true;

        var xhr = new XMLHttpRequest();
        var content = art.content;
        xhr.open("PUT", "http://localhost:8080", false);
        xhr.setRequestHeader("Content-Type", "application/xml; charset=utf-8");
        xhr.setRequestHeader("Accept", "application/xml; charset=utf-8");
        xhr.send(content);
    }
}

chrome.runtime.onMessage.addListener(
    function (message: any) {
        if (message === "Start") {
            console.log(nowb() + message);
            start();
        }
        else if (message === "Cancel") {
            console.log(nowb() + message);
            cancel();
        }
        else if ((<Article>message).isArticle) {
            demandNext = false;
            handleArticle(<Article>message);
            //demandNext = true;
        }
    });

chrome.tabs.onUpdated.addListener(function (tabId, change, tab) {
    if (change.status == "complete" && tab.url.match("^https://getpocket\.com")) {

        thetab = tabId;

        chrome.pageAction.setTitle({ tabId: tabId, title: "Fetch Pocket" });
        chrome.pageAction.show(tabId);

        if (!cancelled && demandNext) {
            chrome.tabs.sendMessage(thetab, "Next");
        }
    }
});
