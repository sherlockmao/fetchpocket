﻿// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

function nowc() {
    var now = new Date();
    return now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
}

class FindNodesofClassFrame {
    node: Node;
    idx: number;
}

console.log("content script load again " + nowc());

class Article {
    isArticle: boolean;
    id: string;
    content: string;

    constructor() {
        this.isArticle = true;
        this.content = null;
    }
}

class ArticleFetcher {
    savedArticles: Object = {}

    pickFirstinQueue(i: number) {
        var queue = document.getElementById("queue");

        if (queue.children.length > i && i >= 0) {
            var li = queue.children[i];

            document.getElementById(li.getAttribute("id")).click();
            return i + 1;
        }
    }

    handlePageChange() {
        var url = window.location;

        if (url.host !== "getpocket.com")
            return;

        if (url.pathname.match(/^\/a\/queue/)) {
            setTimeout(this.parseQueue.bind(this, url), 2000);
        }
        else if (url.pathname.match(/^\/a\/read\/[0-9]+/)) {
            setTimeout(this.parseReader.bind(this, url), 3000);
            
        }
    }

    parseReader(url: Location) {
        console.log("parsing: " + url.href);     
        if (!this.savedArticles[url.href]) {
            var reader = document.getElementById("page_reader");

            var content = <HTMLElement>reader.getElementsByClassName("reader_content_wrapper")[0];

            var art = new Article();
            art.content = content.innerHTML;
            art.isArticle = true;
            var parts = url.pathname.split("/");
            art.id = parts[parts.length - 1];

            chrome.runtime.sendMessage(art);

            this.savedArticles[url.href] = true;
        }
        var li_back = document.getElementById("pagenav_mark");
        li_back.getElementsByTagName("a")[0].click();
    }

    parseQueue(url: Location) {
        var queue = document.getElementById("queue");
        var elem: HTMLElement;
        for (var i = 0; i < queue.childNodes.length; ++i) {
            var li = <HTMLElement>(queue.childNodes[i]);

            var anchors = li.getElementsByTagName("a");

            if (!anchors || !(anchors.length))
                continue;

            var a = anchors[0];

            if (!(a.hasAttribute("href")))
                continue;

            var art_url: string = a.href;
            console.log("art_url: " + art_url);

            if (this.savedArticles[art_url] || !art_url.match(/https:\/\/getpocket\.com\/a\/read\/[0-9]+/)) {
                console.log(art_url + " existed");
                continue;
            }
            elem = li;
            break;
        }
        if (elem) {
            elem.click();
            elem = null;
        }
        else {
            setTimeout(this.handlePageChange.bind(this), 4000);
            window.scrollTo(0, document.body.scrollHeight);
        }
    }
}

var fetcher = new ArticleFetcher();

chrome.runtime.onMessage.addListener(
    function (message: any) {
        if (message === "Next")
            fetcher.handlePageChange();
    });
