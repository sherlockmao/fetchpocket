// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

function nowp() {
    var now = new Date();
    return now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
}

console.log(nowp() + " popups loaded again");

function onSaveArticleClicked() {
    console.log(nowp() + " save button clicked");

    chrome.runtime.sendMessage("Start",
        function (message) {
            var output = document.getElementById("output");
            output.appendChild(document.createTextNode("Started"));
        });
}

function onCancelArticleClicked() {
    chrome.runtime.sendMessage(
        "Cancel",
        function (message) {
            var output = document.getElementById("output");
            output.appendChild(document.createTextNode("Cancelled"));
        });
}

function init_popup() {
    $("#saveArticles").on("click", onSaveArticleClicked);
    $("#cancelSave").on("click", onCancelArticleClicked);
}

window.onload = init_popup;
