﻿using Microsoft.Owin.Hosting;
using System.Collections.Generic;
using Nancy;
using Owin;
using System;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace ArticleSaver
{
    public class HomeModule : NancyModule
    {
        private List<string> articles = new List<string>();

        public HomeModule()
        {
            Put["/", true] = async (ctx, ct) =>
            {
                var byteContent = new Byte[this.Request.Body.Length];
                await this.Request.Body.ReadAsync(byteContent, 0, (int)this.Request.Body.Length);
                var content = System.Text.Encoding.UTF8.GetString(byteContent);
                articles.Add(content);
                await SendviaOutlook(content);
                return "OK";
            };
        }

        private Task SendviaOutlook(string content)
        {
            var t = Task.Run(() =>
            {
                var template = "<!doctype html> <html> <head> <meta charset=\"utf-8\" /> <title>background page of fetch pocket</title> </head> <body> {0} </body> </html>";
                var body = String.Format(template, content);
                var doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(body);
                var h1 = doc.DocumentNode.SelectNodes("//h1");
                string subject = h1[0].InnerText;

                var fromAddress = new MailAddress("KS8xNtK9mds5I@gmail.com", "Niuniu & Xiongxiong");
                var toAddress = new MailAddress("xiaolinhoo@gmail.com", "Sherry");
                const string fromPassword = "bMlgunHSgH0ZOPacPkxwmzB";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    IsBodyHtml = true,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
            });
            return t;
        }
    }

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseNancy();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var url = "http://+:8080";

            using (WebApp.Start<Startup>(url))
            {
                Console.WriteLine("Running on {0}", url);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
            }
        }
    }
}
